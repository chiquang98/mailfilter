/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classifymail;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chiquang
 */
public class CreateDictionary {

    public void readEmail() {
        File folder = new File("data/spam-train");
        File[] listOfFiles = folder.listFiles();
        File output = new File("vocabulary.txt");
        for (int i = 0; i < listOfFiles.length; i++) {
            File file = listOfFiles[i];
            if (file.isFile() && file.getName().endsWith(".txt")) {
                String s = new String();
                FileInputStream input;
                try {
                    input = new FileInputStream(file);
                    DataInputStream stream = new DataInputStream(input);
                    while (stream.available() > 0) {
                        char temp = stream.readChar();
                        System.out.println((char)((int)temp-'0'));
                        s += temp; // đọc một ký tự từ tệp
                        if (temp == ' ') {
                            writeFile(output, s);
                            s="";
                        }
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CreateDictionary.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CreateDictionary.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }

    }

    public void writeFile(File file, String data) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file);
        DataOutputStream stream = new DataOutputStream(outputStream);
        stream.writeChars(data); // ghi một xâu ký tự vào tệp. Nếu ghi từng ký tự thì dùng writeChar(char);
        stream.close();
        outputStream.close();
    }

    public static void main(String[] args) {
        new CreateDictionary().readEmail();
    }

}
